package com.qa

import org.scalatra.ScalatraServlet
import org.scalatra.scalate.ScalateSupport


class MainServlet extends ScalatraServlet with ScalateSupport {
  before() {
    contentType = "text/html"
  }
// edited the file for jenkins push
  get("/") {
    layoutTemplate("/WEB-INF/templates/views/index.ssp")
  }

  get("/dinosaur") {
    layoutTemplate("/WEB-INF/templates/views/dinosaur.ssp")
  }
}
